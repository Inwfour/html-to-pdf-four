var fs = require('fs');
let ejs = require("ejs");
let pdf = require("html-pdf");
let path = require("path");
let data = require('../mock/data.json')
let { toThaiNumber } = require('../../functions')

ejs.renderFile(path.join(path.resolve('../view/'), '.' ,"index.ejs"), {data: data, toThaiNumber: toThaiNumber}, (err, data) => {
  if (err) {

  } else {
       let pathJoin = path.resolve('../').split("\\").join("/")
      let options = {
        // "format": "A4",        // allowed units: A3, A4, A5, Legal, Letter, Tabloid
        // "orientation": "landscape", // portrait or landscape
        width:'11.00in',
        height:'8.50in',
        // base: "file://"+path.join(__dirname,'/'),
        base: "file:///" + pathJoin + "/view/",
      };
      let pathName = (path.resolve('../')).split('\\')
      pdf.create(data, options).toFile(`../${pathName[pathName.length - 1]}.pdf`, function (err, data) {

      });
  }
});